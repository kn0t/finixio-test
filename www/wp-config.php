<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'wp' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', '1337m4st3r' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'XEiE&XEc63}lV]_lfY{KF`I%-Uw D*_KOl1;~!=1]7:f^,nCOlZ}Ct>HkchuD`Oe' );
define( 'SECURE_AUTH_KEY',  'JSQ6(7(o1al(SWT7NBtXwAqGapJB2x#Z*:U;YSur/7)<#&/%K*Y%Ji&j$zFtN/p>' );
define( 'LOGGED_IN_KEY',    '^<,h/_j:`)4KAyZ&>C!TUk-(Kw j^Sb_v5_q3E.aGKY8@CnH)=EsRX(7p7oK7?W3' );
define( 'NONCE_KEY',        '8ZGC<9M@fu`{mkp(Ss0eM{G+#)W7[VFnv]Li0OB8y_mQ2;5TABmdWQ`;4;_ocdo&' );
define( 'AUTH_SALT',        'Y3#8iR6&fc#0(aNxr2]QC$u4^WU7bP63iQg^f,7SDvSIacp2`Z~hIUs:o@,p-DkG' );
define( 'SECURE_AUTH_SALT', 'uvKkEP8UZ+#=[7>4!}>N<.1.v9(`2[3D7UCl0?Kp$yWRJ=j(+2&s.xW|K_.w-<p/' );
define( 'LOGGED_IN_SALT',   '4U(rxsp+)G=ybv#(FWy8PV{,Z*,#b|Z3O*SAHL=VMX:v[FCia8U9d=L9.mAh~5l2' );
define( 'NONCE_SALT',       'mbn!?!G)Iw0NkGnSO@w.lR(_ Fs.5?xY<lMxHpM$8,{fO!U}|9*F6%?ucyS@rsk1' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
