<?php

/*
 * Plugin Name: Finixio Test - Core Plugin
 * Version: 0.1
 * Description: Allows to use the '[finixio-test]' shortcode to show the table required for the test.
 * Author: Filippo Teodori
 * Requires at least: 4.0
 * Tested up to: 5.1
 */

/////////////////////////////////////////////////////////////////////

Class FinixioTest {

	public function __construct() {
		// Add scripts to WordPress queue
		add_action('wp_enqueue_scripts', [$this, "enqueueScripts"]);
		// Register shortcode
		add_shortcode('finixio-test', [$this, "renderShortcode"]);
		return;
	}

	public function enqueueScripts() {
		// Enqueue necessary script
		wp_enqueue_script("coin-providers-list", plugin_dir_url(__FILE__)."js/coin-providers-grid.js", ["jquery"], "1.0");
	}

	public function renderShortcode() {
		// Add relevant data to twig context
		$context 					= 	Timber::get_context();
		$context["current_post"]	=	new Timber\Post();
		$context["assets_url"]		=	plugin_dir_url(__FILE__)."assets/";

		// Return compiled twig template
		return Timber::compile(plugin_dir_path(__FILE__)."/templates/main.twig", $context);
	}

}

/////////////////////////////////////////////////////////////////////


// Instantiate class
$finixio_test_core 	=	new FinixioTest;