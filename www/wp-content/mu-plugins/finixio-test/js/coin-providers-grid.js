jQuery(document).ready(function(){
	window.jQuery = window.$ = jQuery;
	$(".carousel").carousel();
	$('.show-more').click(function(){
	    old_text = $(this).html()
	    if (!$(this).hasClass("opened")) {
	    	$(this).addClass('opened');
	    	new_text = old_text.replace("Show", "Hide");
	    } else {
	    	$(this).removeClass("opened");
	    	new_text = old_text.replace("Hide", "Show");
	    }
	    $(this).html(new_text);
	    icon = $(this).find("i.fa")
	    $(icon).css('transform', ($(icon).css('transform') == 'none') ? 'rotate(180deg)' : 'none');
	});
	$(".carousel-control-next").click(function(){
		$(".carousel").carousel('next')
	});
	$(".carousel-control-prev").click(function(){
		$(".carousel").carousel('prev')
	})
});