<?php
/**
 * The main template file
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
 */

if ( ! class_exists( 'Timber' ) ) {
	echo 'Timber not activated. Make sure you activate the plugin in <a href="/wp-admin/plugins.php#timber">/wp-admin/plugins.php</a>';
	return;
}



$context = Timber::get_context();
$args = array_merge( $wp_query->query_vars, array( 'has_password' => false , 'post_status' => 'publish'  ) );
$context['posts'] = Timber::get_posts($args);
$templates = array( 'index.twig' );



if ( is_home() ) {

	if ( get_query_var( 'paged' ) ) { $paged = get_query_var( 'paged' ); } 
	elseif ( get_query_var( 'page' ) ) { $paged = get_query_var( 'page' ); }
	else { $paged = 1; }

	$args = array(
		'post_type' => 'post',
		'has_password' => false,
		'ignore_sticky_posts' => 1,
		'paged' => $paged,
		'posts_per_page' => 10
	);

	/* THIS LINE IS CRUCIAL */
	/* in order for WordPress to know what to paginate */
	/* your args have to be the default query */
	query_posts($args);
	/* make sure you've got query_posts in your .php file */
	$context['posts'] = Timber::get_posts($args);
	$context['pagination'] = Timber::get_pagination();
	array_unshift( $templates, 'home.twig' );
}

if ( is_front_page() ) {
	
	array_unshift( $templates, 'front-page.twig' );
}


Timber::render( $templates, $context );
