#!/bin/bash
echo "[+] Trying to put sass in watch mode..."
SASSFILE="www/wp-content/themes/0x00/css/scss/style.scss"
if [ -f $SASSFILE ]; then
	echo "[+] Sass is compiling in the background. CTRL+C to halt. Have fun <3"
	while true
	do
		tput sgr 0
		sass $SASSFILE:www/wp-content/themes/0x00/style.css
		sleep 2s
	done
else
	tput setaf 1
	echo "[!] File not found: $SASSFILE"
fi
tput sgr 0